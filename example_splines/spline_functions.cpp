#include "spline_functions.h"
#include "tridag.h"

// Function that finds the second derivatives needed for a cubic spline
void spline(double x[], double y[], int n, double yp_lower, double yp_upper, double y2[])
{
	double a[n],b[n],c[n],F[n];

  // Deal with the lower boundary condition
	if (yp_lower > 0.99e30)
  {
    // User has input a very large value for derivative; assume natural splines
    b[0] = 1.0; // This is free actually.
    c[0] = 0.0;
    F[0] = 0.0;
  }
	else
  {
    // User has input a regular value for derivative; use it
    b[0] = (x[1]-x[0]) / 3.0;
    c[0] = 0.5 * b[0];
    F[0] = (y[1]-y[0]) / (x[1]-x[0]) - yp_lower;
	}

  // Deal with the upper boundary condition
	if (yp_upper > 0.99e30)
  {
    // User has input a very large value for derivative; assume natural splines
    b[n-1] = 1.0; // This is free actually.
    a[n-1] = 0.0;
    F[n-1] = 0.0;
  }
	else
  {
    // User has input a regular value for derivative; use it
    b[n-1] = (x[n-1]-x[n-2]) / 3.0;
    a[n-1] = 0.5 * b[n-1];
    F[n-1] = yp_upper - (y[n-1]-y[n-2]) / (x[n-1]-x[n-2]);
	}

  // Set up the rest of the matrix
	for (int i=1; i<=n-2; i++)
  {
    a[i] = (x[i] - x[i-1]) / 6.0;
    b[i] = (x[i+1] - x[i-1]) / 3.0;
    c[i] = (x[i+1] - x[i]) / 6.0;
    F[i] = (y[i+1]-y[i]) / (x[i+1]-x[i]) - (y[i]-y[i-1]) / (x[i]-x[i-1]);
	}

  // Invert the matrix to get the y2 values
  tridag(a, b, c, F, y2, n);

}

// Function that evaluates a cubic spline
double splint(double x[], double y[], double y2[], int n, double xval)
{
  // Variables used for finding the correct interval to use
	int klo = 0;
  int khi = n-1;
  int k;

  // Variables A and B from Eq 5 in the notes, and h the width in x of the relevant interval.
	double A,B,h;

  // Find the interval that contains xval, by bisection
	while (khi-klo > 1)
  {
    // Take the average of lower and upper index, dividing by 2 with bitwise right shift operator >>
		k=(khi+klo) >> 1;
    // If x at the middle index k is greater than xval, lower the upper limit to the midpoint
		if (x[k] > xval)
    {
      khi=k;
    }
    // ...otherwise raise the lower limit to the midpoint
		else
    {
      klo=k;
    }
	}

  // Calculate the width of the interval in x
	h=x[khi]-x[klo];

  // Balk if the interval has zero width
	if (h == 0.0)
  {
    std::cout << "Bad x vector input to routine splint" << std::endl;
    exit(1);
  }

  // Evaluate coefficients A and B
	A=(x[khi]-xval)/h;
	B=(xval-x[klo])/h;

  // Evaluate y using y2, A and B to get C and D from Eq 5 in the notes
	return A*y[klo] + B*y[khi] + (h*h)/6.0*((A*A*A-A)*y2[klo] + (B*B*B-B)*y2[khi]);

}
