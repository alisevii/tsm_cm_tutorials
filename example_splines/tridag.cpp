#include "tridag.h"

// Tridiagonal matrix solver
void tridag(double a[], double b[], double c[], double r[], double u[],	int n)
{
	int j;
	double bet,gam[n];

	if (b[0] == 0.0)
  {
    std::cout << "Error 1 in tridag" << std::endl;
    exit(1);
  }
  bet = bet=b[0];
	u[0] = r[0]/bet;
	for (j=1; j<=n-1; j++)
  {
		gam[j] = c[j-1]/bet;
		bet = b[j] - a[j] * gam[j];
		if (bet == 0.0)
    {
      std::cout << "Error 1 in tridag" << std::endl;
      exit(1);
    }
		u[j] = (r[j]-a[j]*u[j-1]) / bet;
	}
	for (j=n-2; j>=0; j--)
  {
		u[j] -= gam[j+1] * u[j+1];
  }
}
