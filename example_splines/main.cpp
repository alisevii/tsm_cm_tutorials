// Cubic splines tutorial
// Code adapted from Numerical Recipes in C (2nd Ed)
// Note that all indices are C-style, starting at 0!
//
// Pat Scott Nov 22 2015
//
// Edited and separated functions by Alise Nov 24 2015

// Include some basic C/C++ standard library routines
#include <iostream>
#include <cstdlib>
#include "spline_functions.h"		//let the compiler know where to look for the functions spline() and splint() from the main routine

// The main program.  This is the one that runs when you run the executable that
// is created when you compile the code.  Must return int.
int main()
{

  // Just hardcode the input data for simplicity
  double x[11] = {-2.1, -1.45, -1.3, -0.2, 0.1, 0.15, 0.8, 1.1, 1.5, 2.8, 3.8};
  double y[11] = {0.012155178329914935, 0.12215066953999, 0.18451952399298924,
                  0.96078943915232318, 0.99004983374916811, 0.97775123719333634,
                  0.52729242404304855, 0.29819727942988733, 0.10539922456186433,
                  0.00039366904065507862, 5.3553478027931087e-07};

  // Declare the array that will hold the second derivatives
  double y2[11];

  // Set up the spline, i.e. calculate the second derivatives
  spline(x, y, 11, 0.0, 0.0, y2);

	//Ask user to input an x value for interpolation
	std::cout << "This routine performs cubic spline interpolation using zero first derivative boundary conditions." << std::endl;
	std::cout << "Please enter an x value for interpolation:" << std::endl;
	double x_in;
	std::cin >> x_in;
	//Evaluate and print to screen interpolated value
	std::cout << "The interpolated y value is:" << std::endl;
	std::cout << splint(x,y,y2,11,x_in) << std::endl;


///	Pat's main for Monday tutorial
/*
  // The smallest and largest x values that you want to tabulate y for
  double xmin = -4;//x[0];
  double xmax = 4;//x[10];

  // The number of x values you want to spit out y values for
  int numx = 500;


  // Loop over numx x values and evaluate the spline, using the second derivates
  std::cout << "#x      y" << std::endl;
  for (int i = 0; i <= numx; i++)
  {
    double xval = xmin + double(i)/double(numx)*(xmax-xmin);
    double yval = splint(x, y, y2, 11, xval);
    std::cout << xval << " " << yval << std::endl;
  }
*/

  // Return 0 to indicate successful completion
  return 0;

}


